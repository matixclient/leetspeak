package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.player.PlayerSendChatMessageEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Lars on 08.05.2016.
 */
public class ModuleLeetSpeak extends Module {
  private static HashMap<String, String> replacementChars = new HashMap<>();

  static {
    replacementChars.put("a", "4");
    replacementChars.put("b", "8");
    replacementChars.put("c", "(");
    replacementChars.put("e", "3");
    replacementChars.put("g", "6");
    replacementChars.put("i", "!");
    replacementChars.put("l", "1");
    replacementChars.put("o", "0");
    replacementChars.put("q", "9");
    replacementChars.put("s", "5");
    replacementChars.put("t", "7");
    replacementChars.put("x", "+");
    replacementChars.put("z", "2");
  }

  public ModuleLeetSpeak() {
    super("LeetSpeak", ModuleCategory.OTHER);

    this.setVersion("1.0");
    this.setBuildVersion(15801);
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);
  }

  @EventHandler
  public void sendChat(PlayerSendChatMessageEvent chatMessageEvent) {
    if (!chatMessageEvent.getChatMessage().startsWith("/")) {
      String chatMessage = chatMessageEvent.getChatMessage();

      for (Map.Entry<String, String> replacementEntry : replacementChars.entrySet()) {
        chatMessage = chatMessage.replaceAll("(?i)" + replacementEntry.getKey(), replacementEntry.getValue());
      }

      chatMessageEvent.setChatMessage(chatMessage);
    }
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);
  }
}
